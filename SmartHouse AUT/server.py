import socket               # Import socket module
from lib import *
import pickle
import pymysql
import time


def connect_to_bd():
  try:
    connection = pymysql.connect(
      host='localhost',
      user='root',
      port = 3306,
      password='root',
      database='slave_bd',
      cursorclass=pymysql.cursors.DictCursor
    )
    print("Successfully connected")
    return connection

  except Exception as ex:
    print("Connection refused")
    print("Type Error: " + str(ex))

def LogIn(connection, emai, password):
   with connection.cursor() as cursor:
      insert_query = ("SELECT * FROM user WHERE (emai = %s AND password=%s )")
      cursor.execute(insert_query, (emai, password))
      rows = cursor.fetchall()
      if not rows:
          return False
      else:
          return True


def Registration(connection, name, surname,  email,phone_number, password):
   with connection.cursor() as cursor:
      insert_query = ("INSERT INTO user (name, surname, emai, phone_number ,password ) VALUES (%s,%s,%s,%s,%s)")
      cursor.execute(insert_query, ( name, surname, email, phone_number, password))
      connection.commit()

def CheckEmail(connection, email):
   with connection.cursor() as cursor:
      insert_query = ("SELECT emai FROM user WHERE emai = %s")
      cursor.execute(insert_query, email)
      rows = cursor.fetchall()
      if not rows:
          return True
      else:
          return False

def AddDevice (connection, name, type_of_device, location,email, status):
        with connection.cursor() as cursor:
            insert_query = ("INSERT INTO device (device_name , device_type, location, email, status) VALUES (%s,%s,%s,%s,%s) ")
            cursor.execute(insert_query, (name, type_of_device, location,email, status))
            connection.commit()
            return True

def ShowDeviceList(connection, Email):
    with connection.cursor() as cursor:
        insert_query = ("SELECT device_name, device_type , location, status FROM device WHERE email = %s")
        cursor.execute(insert_query, Email)
        rows = cursor.fetchall()
        if not rows:
            return False
        else:
            return rows

def OFFDevice(connection, name, email, ):
    with connection.cursor() as cursor:
        insert_query = ("UPDATE device SET status = 'Викл.' WHERE device_name = %s and email = %s")
        cursor.execute(insert_query, (name, email))
        connection.commit()
        return True

def ONDevice(connection, name, email, ):
    with connection.cursor() as cursor:
        insert_query = ("UPDATE device SET status = 'Вкл.' WHERE device_name = %s and email = %s")
        cursor.execute(insert_query, (name, email))
        connection.commit()
        return True

def DELETEDevice(connection, name, email, ):
    with connection.cursor() as cursor:
        insert_query = ("DELETE FROM device WHERE (device_name = %s AND email= %s)")
        cursor.execute(insert_query, (name, email))
        connection.commit()
        return True

host = '127.0.0.1' # Get local machine name
port = 65534              # Reserve a port for your service.
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  # Create a socket object
s.bind((host, port))  # Bind to the port
s.listen()
print('socket is listening')
while True:
   c, addr = s.accept()  # Establish connection with client.
   print('CONNECTED')  # Now wait for client connection.
   try:
       bytes = c.recv(65000)
       command = pickle.loads(bytes)
       print(command.instraction)

       if command.instraction == 'LogIn':
          bytes = c.recv(65000)
          logIn = pickle.loads(bytes)
          accept = LogIn(connect_to_bd(), logIn.email, logIn.password)
          c.sendall(pickle.dumps(accept))


       elif command.instraction == 'Registration':
           bytes = c.recv(65000)
           user = pickle.loads(bytes)
           Registration(connect_to_bd(), user.name, user.surname, user.email, user.phone_number, user.password)

       elif command.instraction == 'CheckEmail':
          bytes = c.recv(65000)
          check = pickle.loads(bytes)
          accept = CheckEmail(connect_to_bd(), check.email)
          c.sendall(pickle.dumps(accept))

       elif command.instraction == 'AddDevice':
           bytes = c.recv(65000)
           add_device = pickle.loads(bytes)
           accept = AddDevice(connect_to_bd(), add_device.name, add_device.type_of_device, add_device.location, add_device.email, add_device.status)
           c.sendall(pickle.dumps(accept))


       elif command.instraction == 'ShowDeviceList':
           bytes = c.recv(65000)
           emails = pickle.loads(bytes)
           orders = ShowDeviceList(connect_to_bd(), emails.email)
           c.sendall(pickle.dumps(orders))

       elif command.instraction == 'OFFDevice':
          bytes = c.recv(65000)
          info = pickle.loads(bytes)
          accept = OFFDevice(connect_to_bd(), info.name, info.email)
          c.sendall(pickle.dumps(accept))

       elif command.instraction == 'ONDevice':
          bytes = c.recv(65000)
          info = pickle.loads(bytes)
          accept = ONDevice(connect_to_bd(), info.name, info.email)
          c.sendall(pickle.dumps(accept))

       elif command.instraction == 'DELETEDevice':
          bytes = c.recv(65000)
          info = pickle.loads(bytes)
          accept = DELETEDevice(connect_to_bd(), info.name, info.email)
          c.sendall(pickle.dumps(accept))

       print('End Command')
   except EOFError:
       print('Waiting for connection EOFError')
   except ConnectionResetError:
       print('Waiting for connection ConnectionResetError')
