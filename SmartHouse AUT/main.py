from PyQt5 import QtWidgets, QtGui
from PyQt5.QtWidgets import QMessageBox

from GUI.GUI import Ui_Login, Ui_Registration, Ui_DevicesList, Ui_AddDevice, Ui_Device_Otpions
from lib import *
import socket
import pickle
import time
import sys
import validators
try:
    host = '127.0.0.1' # Get local machine name
    port = 65534       # Reserve a port for your service.

    class LogWindow(QtWidgets.QMainWindow):
        def __init__(self):
            super().__init__()
            self.sign_in_gui = Ui_Login()
            self.sign_in_gui.setupUi(self)
            self.sign_in_gui.password_line.setEchoMode(QtWidgets.QLineEdit.Password)
            self.init_UI_LogWindow()

        def reg_btn_clicked(self):
            self.close()
            self.regwindow = RegWindow()
            self.regwindow.show()

        def login_btn_clicked(self):#НЕГОТОВО ДОРОБИТИ
            try:
                email = self.sign_in_gui.login_line.text()
                password = self.sign_in_gui.password_line.text()

                valid_email = validators.email(email)
                valid_password = validators.truthy(password)

                if valid_email == True and valid_password == True:
                    try:
                        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  # Create a socket object
                        s.connect((host, port))
                        print('Connect succesful')
                    except ConnectionRefusedError:
                        back = QMessageBox()
                        back.setWindowTitle('Помилка')
                        back.setText("Немає з'єднання")
                        back.setIcon(QMessageBox.Warning)
                        back.setStandardButtons(QMessageBox.Ok)
                        back.exec_()
                    command = Commands('LogIn')
                    login = ClientLogIn(email, password)

                    # login
                    s.sendall(pickle.dumps(command))
                    time.sleep(0.01)
                    s.sendall(pickle.dumps(login))
                    bytes = s.recv(65000)
                    accept = pickle.loads(bytes)
                    s.close()

                    if accept==False:
                        back = QMessageBox()
                        back.setWindowTitle('Помилка!')
                        back.setText("Неправильний логін або пароль.")
                        back.setIcon(QMessageBox.Warning)
                        back.setStandardButtons(QMessageBox.Ok)
                        back.exec_()

                    elif accept == True:
                        self.close()
                        self.userwindow = DevicesListWindow(email)
                        self.userwindow.show()

                else:
                    back = QMessageBox()
                    back.setWindowTitle('Помилка!')
                    back.setText("Неправильний логін або пароль.")
                    back.setIcon(QMessageBox.Warning)
                    back.setStandardButtons(QMessageBox.Ok)
                    back.exec_()
            except OSError:
                    print('Invalid connection')

        def init_UI_LogWindow(self):
            self.sign_in_gui.registration_btn.clicked.connect(self.reg_btn_clicked)
            self.sign_in_gui.login_btn.clicked.connect(self.login_btn_clicked)



    class RegWindow(QtWidgets.QMainWindow):
        def __init__(self):
            super().__init__()
            self.reg_gui = Ui_Registration()
            self.reg_gui.setupUi(self)
            self.reg_gui.password_line.setEchoMode(QtWidgets.QLineEdit.Password)
            self.init_UI_RegWindow()

        def registration(self):
            try:
                name = self.reg_gui.name_line.text()
                surname = self.reg_gui.surname_line.text()
                email = self.reg_gui.email_line.text()
                phone_number = self.reg_gui.phone_number_line.text()
                password = self.reg_gui.password_line.text()

                # valid_name = validators.truthy(name)
                # valid_surname = validators.truthy(surname)
                valid_email = validators.email(email)
                # valid_phone_number = validators.truthy(phone_number)
                valid_password = validators.length(password, min=8)


                if name.isalpha() == True and surname.isalpha() == True and valid_email == True and phone_number.isdigit() == True and valid_password == True:
                        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  # Create a socket object
                        s.connect((host, port))
                        user = ClientRegistration(name, surname, email, phone_number, password)
                        check_email = CheckEmail(email)

                        command_chek = Commands('CheckEmail')
                        s.send(pickle.dumps(command_chek))
                        time.sleep(0.01)
                        s.send(pickle.dumps(check_email))
                        bytes = s.recv(65000)
                        accept = pickle.loads(bytes)
                        s.close()


                        if accept == True:
                            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                            s.connect((host, port))
                            command = Commands('Registration')
                            s.send(pickle.dumps(command))
                            time.sleep(0.01)
                            s.send(pickle.dumps(user))  # REGISTRSTION!!!!!!!!!!
                            self.close()
                            application.show()
                        else:
                            back = QMessageBox()
                            back.setWindowTitle('Помилка!')
                            back.setText("Користувач з таким емейлом уже існує.")
                            back.setIcon(QMessageBox.Warning)
                            back.setStandardButtons(QMessageBox.Ok)
                            back.exec_()
                elif valid_email != True:
                    back = QMessageBox()
                    back.setWindowTitle('Помилка!')
                    back.setText("Некоректний емейл.")
                    back.setIcon(QMessageBox.Warning)
                    back.setStandardButtons(QMessageBox.Ok)
                    back.exec_()
                elif name.isalpha() == False:
                    back = QMessageBox()
                    back.setWindowTitle('Помилка!')
                    back.setText("Ім'я має містити тільки літери.")
                    back.setIcon(QMessageBox.Warning)
                    back.setStandardButtons(QMessageBox.Ok)
                    back.exec_()
                elif surname.isalpha()== False:
                    back = QMessageBox()
                    back.setWindowTitle('Помилка!')
                    back.setText("Прізвище має містити тільки літери.")
                    back.setIcon(QMessageBox.Warning)
                    back.setStandardButtons(QMessageBox.Ok)
                    back.exec_()
                elif phone_number.isdigit() == False:
                    back = QMessageBox()
                    back.setWindowTitle('Помилка!')
                    back.setText("Номер телефону має містити тільки цифри.")
                    back.setIcon(QMessageBox.Warning)
                    back.setStandardButtons(QMessageBox.Ok)
                    back.exec_()
                else:
                    back = QMessageBox()
                    back.setWindowTitle('Помилка!')
                    back.setText("Пароль має містити щонайменше 8 симолів.")
                    back.setIcon(QMessageBox.Warning)
                    back.setStandardButtons(QMessageBox.Ok)
                    back.exec_()

            except OSError:
                    print('Invalid connection')

        def to_login(self):
            self.close()
            self.login = LogWindow()
            self.login.show()

        def init_UI_RegWindow(self):
            self.reg_gui.create_ac_btn.clicked.connect(self.registration)
            self.reg_gui.login_btn.clicked.connect(self.to_login)



    class DevicesListWindow(QtWidgets.QMainWindow):
        def __init__(self, email):
            super().__init__()
            DevicesListWindow.email = email
            self.dev_gui = Ui_DevicesList()
            self.dev_gui.setupUi(self)
            # self.dev_gui.tableWidget.setItem(1, 1, QtWidgets.QTableWidgetItem("text1"))
            self.init_UI_DevicesListWindow()

        def add_devices(self):
            self.close()
            self.addwindow = AddDevicesWindow(DevicesListWindow.email)
            self.addwindow.show()

        def options_devices(self):
                self.close()
                self.opt = OptionsWindow(DevicesListWindow.email)
                self.opt.show()

        def list_of_device(self):
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.connect((host, port))
            command = Commands('ShowDeviceList')
            email = Email(DevicesListWindow.email)
            s.sendall(pickle.dumps(command))
            time.sleep(0.01)
            s.sendall(pickle.dumps(email))
            bytes = s.recv(65000)
            lists = pickle.loads(bytes)
            s.close()
            print(lists)
            if lists == False:
                print('Активні пристрої відсутні')
            else:
                for i in lists:
                    rowPosition = self.dev_gui.tableWidget.rowCount()
                    self.dev_gui.tableWidget.insertRow(rowPosition)
                    self.dev_gui.tableWidget.setItem(rowPosition, 0, QtWidgets.QTableWidgetItem(i.get('location')))
                    self.dev_gui.tableWidget.setItem(rowPosition, 1, QtWidgets.QTableWidgetItem(i.get('device_name')))
                    self.dev_gui.tableWidget.setItem(rowPosition, 2, QtWidgets.QTableWidgetItem(i.get('status')))

        def init_UI_DevicesListWindow(self):
            self.list_of_device()
            self.dev_gui.list_btn.clicked.connect(self.add_devices)
            self.dev_gui.option_btn.clicked.connect(self.options_devices)


    class AddDevicesWindow(QtWidgets.QMainWindow):
        def __init__(self, email):
            super().__init__()
            AddDevicesWindow.email = email
            self.add_gui = Ui_AddDevice()
            self.add_gui.setupUi(self)
            self.init_UI_AddDevicesWindow()

        def go_back(self):
            self.close()
            self.devwindow = DevicesListWindow(AddDevicesWindow.email)
            self.devwindow.show()

        def confirm(self):
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.connect((host, port))

            name = self.add_gui.device_name_line.text()
            type = self.add_gui.device_type_line.text()
            location = self.add_gui.location_line.currentText()
            status = self.add_gui.location_line_2.currentText()


            valid_name = validators.truthy(name)
            valid_type = validators.truthy(type)
            valid_location = validators.truthy(location)


            if valid_name == True and valid_type == True and valid_location == True :
                command = Commands('AddDevice')
                addDevice = AddDevice(name, type, location, AddDevicesWindow.email, status)

                s.sendall(pickle.dumps(command))
                time.sleep(0.01)
                s.sendall(pickle.dumps(addDevice))
                bytes = s.recv(65000)
                accept = pickle.loads(bytes)
                s.close()
                if accept == True:
                    back = QMessageBox()
                    back.setWindowTitle('!')
                    back.setText("Пристрій додано")
                    back.setIcon(QMessageBox.Information)
                    back.setStandardButtons(QMessageBox.Ok)
                    back.buttonClicked.connect(self.go_back)
                    back.exec_()


            else:
                command = Commands('Wait')
                s.sendall(pickle.dumps(command))
                back = QMessageBox()
                back.setWindowTitle('Помилка!')
                back.setText("Некоректно заповнені поля!")
                back.setIcon(QMessageBox.Warning)
                back.setStandardButtons(QMessageBox.Ok)
                back.exec_()

        def init_UI_AddDevicesWindow(self):
            self.add_gui.ok_btn.clicked.connect(self.confirm)
            self.add_gui.dismiss_btn.clicked.connect(self.go_back)

    class OptionsWindow(QtWidgets.QMainWindow):
        def __init__(self, email):
            super().__init__()
            OptionsWindow.email = email
            self.opt = Ui_Device_Otpions()
            self.opt.setupUi(self)
            self.init_UI_OptionsWindow()

        def go_back(self):
            self.close()
            self.devwindow = DevicesListWindow(OptionsWindow.email)
            self.devwindow.show()

        def combo_box_of_devices(self):
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.connect((host, port))
            command = Commands('ShowDeviceList')
            email = Email(DevicesListWindow.email)
            s.sendall(pickle.dumps(command))
            time.sleep(0.01)
            s.sendall(pickle.dumps(email))
            bytes = s.recv(65000)
            lists = pickle.loads(bytes)
            s.close()
            if lists == False:
                print('Активні пристрої відсутні')
            else:
                for i in lists:
                    self.opt.device_box.addItem(i.get('device_name'))

        def off_device(self):
            name = self.opt.device_box.currentText()
            if name == '':
                back = QMessageBox()
                back.setWindowTitle('Опції')
                back.setText("Немає активних пристроїв")
                back.setIcon(QMessageBox.Information)
                back.setStandardButtons(QMessageBox.Ok)
                back.exec_()
            else:
                s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                s.connect((host, port))
                command = Commands('OFFDevice')
                email = OffDevice(name, OptionsWindow.email)
                s.sendall(pickle.dumps(command))
                time.sleep(0.01)
                s.sendall(pickle.dumps(email))
                bytes = s.recv(65000)
                accept = pickle.loads(bytes)
                s.close()
                if accept == True:
                    back = QMessageBox()
                    back.setWindowTitle('Опції')
                    back.setText("Пристрій вимкнено!")
                    back.setIcon(QMessageBox.Information)
                    back.setStandardButtons(QMessageBox.Ok)
                    back.exec_()

        def on_device(self):
            name = self.opt.device_box.currentText()
            if name == '':
                back = QMessageBox()
                back.setWindowTitle('Опції')
                back.setText("Немає активних пристроїв")
                back.setIcon(QMessageBox.Information)
                back.setStandardButtons(QMessageBox.Ok)
                back.exec_()
            else:
                s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                s.connect((host, port))
                command = Commands('ONDevice')
                email = OffDevice(name, OptionsWindow.email)
                s.sendall(pickle.dumps(command))
                time.sleep(0.01)
                s.sendall(pickle.dumps(email))
                bytes = s.recv(65000)
                accept = pickle.loads(bytes)
                s.close()
                if accept == True:
                    back = QMessageBox()
                    back.setWindowTitle('Опції')
                    back.setText("Пристрій увімкено!")
                    back.setIcon(QMessageBox.Information)
                    back.setStandardButtons(QMessageBox.Ok)
                    back.exec_()

        def delete_device(self):
            name = self.opt.device_box.currentText()
            if name == '':
                back = QMessageBox()
                back.setWindowTitle('Опції')
                back.setText("Немає активних пристроїв")
                back.setIcon(QMessageBox.Information)
                back.setStandardButtons(QMessageBox.Ok)
                back.exec_()
            else:
                s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                s.connect((host, port))
                command = Commands('DELETEDevice')
                email = OffDevice(name, OptionsWindow.email)
                s.sendall(pickle.dumps(command))
                time.sleep(0.01)
                s.sendall(pickle.dumps(email))
                bytes = s.recv(65000)
                accept = pickle.loads(bytes)
                s.close()
                self.close()
                self.opt = OptionsWindow(DevicesListWindow.email)
                self.opt.show()

                if accept == True:
                    back = QMessageBox()
                    back.setWindowTitle('Опції')
                    back.setText("Пристрій видалено!")
                    back.setIcon(QMessageBox.Information)
                    back.setStandardButtons(QMessageBox.Ok)
                    back.exec_()

        def go_to_dev(self):
            self.close()
            self.devwindow = DevicesListWindow(OptionsWindow.email)
            self.devwindow.show()

        def init_UI_OptionsWindow(self):
            self.combo_box_of_devices()
            self.opt.back_btn.clicked.connect(self.go_to_dev)
            self.opt.off_btn.clicked.connect(self.off_device)
            self.opt.on_btn.clicked.connect(self.on_device)
            self.opt.delete_btn.clicked.connect(self.delete_device)

    app = QtWidgets.QApplication([])
    application = LogWindow()
    application.show()
    sys.exit(app.exec_())
except:
    print('wrong smth')

