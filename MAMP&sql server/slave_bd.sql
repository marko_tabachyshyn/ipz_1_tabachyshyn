-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 29, 2021 at 04:52 PM
-- Server version: 5.7.24
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `slave_bd`
--

-- --------------------------------------------------------

--
-- Table structure for table `device`
--

CREATE TABLE `device` (
  `device_id` int(11) NOT NULL,
  `device_name` varchar(50) NOT NULL,
  `device_type` varchar(50) NOT NULL,
  `location` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `device`
--

INSERT INTO `device` (`device_id`, `device_name`, `device_type`, `location`, `email`, `status`) VALUES
(13, 'Чайник', 'Чайник', 'Кухня', 'markoman@gmail.com', 'Вкл.'),
(14, 'Богун', 'Слейв', 'Вітальня', 'markoman@gmail.com', 'Вкл.');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `surname` varchar(50) NOT NULL,
  `emai` varchar(50) NOT NULL,
  `phone_number` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `name`, `surname`, `emai`, `phone_number`, `password`) VALUES
(1, 'asdasdasdasdasdasd', 'sadasdasdasdasdas', 'sadasdasdasdasdas@sfdf.com', 'asdasdasdasdasdasd', '11111111'),
(2, 'loxloxloxloxlox', 'loxloxloxloxlox', 'loxloxloxloxlox@wqwewq.cocm', 'loxloxloxloxlox', 'loxloxloxloxloxloxloxlox'),
(3, 'Vladyslav', 'Vladyslav', 'Vladyslav@Vladyslav.Vladyslav', 'VladyslavVladyslav', 'VladyslavVladyslav'),
(4, 'VladyslavVladyslav', 'VladyslavVladyslav', 'Vladyslav@Vladyslav.VladyslavVladyslav', 'VladyslavVladyslavVladyslavVladyslav', 'VladyslavVladyslav'),
(5, 'Марко', 'Табачишин', 'markoman@gmail.com', '+380635197392', 'markoman777'),
(6, 'fuffuffufuf', 'gykyguyg', 'yyddiytdd@gmail.com', '0635197392', 'asdfghjkl'),
(7, 'asdasdasdasd', 'saadasdasd', 'asdasdasdas@gmail.com', '5565116156565', 'asdasvvvvvvv'),
(8, 'asdasasddas', 'asdasasdasda', 'asdasadsdad@ssdf.com', '453543453534', 'scdSDGSDGSG');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `device`
--
ALTER TABLE `device`
  ADD PRIMARY KEY (`device_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `device`
--
ALTER TABLE `device`
  MODIFY `device_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
